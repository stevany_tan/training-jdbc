/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.impl;

import com.DAO.CategoryDAO;
import com.model.Category;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Altaire
 */
public class CategoryDAOImpl implements CategoryDAO{
    private Connection connection;
    public CategoryDAOImpl(Connection connection){
        this.connection = connection;
    }
    @Override
    public int save(Category category) throws SQLException {
            String sql = "INSERT INTO category(name) VALUE(?) " ;
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, category.getName());
            
            return statement.executeUpdate();
            
    }

    @Override
    public int update(int id, Category category) throws SQLException {
        String sql = "UPDATE category c SET name = ? WHERE category_id = ?";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, category.getName());
        statement.setInt(2, id);
        return statement.executeUpdate();
    }

    @Override
    public int delete(int id) throws SQLException {
      String sql = "DELETE FROM category WHERE category_id = ?";
      PreparedStatement statement = connection.prepareStatement(sql);
      statement.setInt(1, id);
      return statement.executeUpdate();
    }

    @Override
    public Category findByid(int id) throws SQLException {
       String sql = "SELECT c.category_id, c.name FROM category c where category_id = ? ";
       PreparedStatement statement = connection.prepareStatement(sql);
       statement.setInt(1, id);
       Category result = null;
       ResultSet rs = statement.executeQuery();
       if(rs.next()){
           result = new Category();
           result.setId(rs.getInt("category_id"));
           result.setName(rs.getString("name"));
       }return result;
    }

    @Override
    public List<Category> findAll() throws SQLException {
       String sql = "SELECT c.category_id, c.name FROM category c";
       PreparedStatement statement = connection.prepareStatement(sql);
       ResultSet rs = statement.executeQuery();
       List<Category> result = new ArrayList<Category>();
       while(rs.next()){
           Category cat = new Category();
           cat.setId(rs.getInt("category_id"));
           cat.setName(rs.getString("name"));
           result.add(cat);
       }
       return result;
    }

    @Override
    public List<Category> findByName(String name) throws SQLException {
        String sql = "SELECT c.category_id, c.name from category c where c.name like ? ";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, "%" + name + "%");
        ResultSet rs = statement.executeQuery();
        List<Category> result = new ArrayList<Category>();
        while(rs.next()){
            Category cat = new Category();
            cat.setId(rs.getInt("category_id"));
            cat.setName(rs.getString("name"));
            result.add(cat);
        }return result;
    }
// private Connection connection;
 
     

}
