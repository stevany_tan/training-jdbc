/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.impl;

import com.DAO.ProductDAO;
import com.model.Category;
import com.model.Product;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author Altaire
 */
public class ProductDAOImp implements ProductDAO {

    private Connection connection;

    public ProductDAOImp(Connection connection) {
        this.connection = connection;

    }

    @Override
    //rapihin alt shift + f, shift enter, utk pindah baris
    public int save(Product product) throws SQLException {
        String sql = "INSERT INTO product (name, description, price, expiredDate, category_id) values(?,?,?,?,?)";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, product.getName());
        statement.setString(2, product.getDescription());
        statement.setDouble(3, product.getPrice());
        statement.setDate(4, new java.sql.Date(product.getExpiredDate().getTime()));
        statement.setInt(5, product.getCategory().getId());
        return statement.executeUpdate();
    }

    @Override
    public int update(int id, Product product) throws SQLException {
        String sql = "UPDATE product SET name = ? , description = ?, price = ?, expiredDate = ?, category_id = ?  WHERE product_id = ? ";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, product.getName());
        statement.setString(2, product.getDescription());
        statement.setDouble(3, product.getPrice());
        statement.setDate(4, new java.sql.Date (product.getExpiredDate().getDate()));
        statement.setInt(5, product.getCategory().getId());
        
        statement.setInt(6, id);
        return statement.executeUpdate();
    }

    @Override
    public int delete(int id) throws SQLException {
      String sql = "DELETE FROM product WHERE product_id =  ? ";
      PreparedStatement statement = connection.prepareStatement(sql);
      statement.setInt(1, id);
      return statement.executeUpdate();
              
    }

    @Override
    public Product findById(int id) throws SQLException {
        String sql = "SELECT p.product_id,p.`name`,p.description,p.price,p.`expiredDate`,p.category_id,c.`name` as category_name "
                + " FROM product p INNER JOIN category c ON p.category_id = c.category_id where p.product_id like ? ";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(1, id);
        ResultSet rs = statement.executeQuery();
        Product result = null;
        if (rs.next()) {
            Category c = new Category();
            c.setId(rs.getInt("category_id"));
            c.setName(rs.getString("category_name"));

            Product p = new Product();
            p.setCategory(c);
            p.setId(rs.getInt("product_id"));
            p.setName(rs.getString("name"));
            p.setDescription(rs.getString("description"));
            p.setPrice(rs.getDouble("price"));
            Calendar cal = Calendar.getInstance();
            p.setExpiredDate(rs.getDate("expiredDate"));
            result = p;

        } return result;
    }

    @Override
    public List<Product> findAll() throws SQLException {
        String sql = "SELECT p.product_id,p.`name`,p.description,p.price,p.`expiredDate`,p.category_id,c.`name` as category_name "
                + " FROM product p INNER JOIN category c ON p.category_id = c.category_id order by p.product_id asc";
        PreparedStatement statement = connection.prepareStatement(sql);
        ResultSet rs = statement.executeQuery();
        List<Product> result = new ArrayList<Product>();
        while (rs.next()) {
            Category c = new Category();
            c.setId(rs.getInt("category_id"));
            c.setName(rs.getString("category_name"));

            Product p = new Product();
            p.setCategory(c);
            p.setId(rs.getInt("product_id"));
            p.setName(rs.getString("name"));
            p.setDescription(rs.getString("description"));
            p.setPrice(rs.getDouble("price"));
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
          /*  Calendar cal = Calendar.getInstance();
            p.setExpiredDate(cal.getTime());*/
            p.setExpiredDate(rs.getDate("expiredDate"));
            result.add(p);


        }
        return result;
    }

    @Override
    public List<Product> findByName(String name) throws SQLException {
        String sql = "SELECT p.product_id,p.`name`,p.description,p.price,p.`expiredDate`,p.category_id,c.`name` as category_name "
                + " FROM product p INNER JOIN category c ON p.category_id = c.category_id where p.name like ? ";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, "%" + name + "%");
        List<Product> result = new ArrayList<Product>();
        ResultSet rs = statement.executeQuery();
        while (rs.next()) {
            Category c = new Category();
            c.setId(rs.getInt("category_id"));
            c.setName(rs.getString("category_name"));

            Product p = new Product();
            p.setCategory(c);
            p.setId(rs.getInt("product_id"));
            p.setName(rs.getString("name"));
            p.setDescription(rs.getString("description"));
            p.setPrice(rs.getDouble("price"));
            p.setExpiredDate(rs.getDate("expiredDate"));

            result.add(p);
        }
        return result;
    }
}
